import express from "express";
import dotenv from "dotenv";
dotenv.config({ path: "./Config/config.env" });
import categoriesRoutes from "./Routes/category";
import bookRoutes from "./Routes/book";
import { logger } from "./Middleware/logger";
import errorHandler from "./Middleware/error";
import { connectDB } from "./Config/db";

const app = express();

connectDB();

const PORT = process.env.PORT;

app.use(express.json());
app.use(logger);
app.use("/category", categoriesRoutes);
app.use("/book", bookRoutes);
app.use(errorHandler);
app.listen(PORT, () => console.log(`Экспресс сервэр ${PORT} порт дээр аслаа`));
