export type TCategory = {
  name: string;
  description: string;
  photo: string;
  averateRating: number;
  averagePrice: number;
  createdAt: Date;
  slug: string;
};

export type TBook = {
  name: string;
  price: number;
  author?: string;
  imageUrl: string;
  rating?: number;
  description: string;
  categoryId: string;
  totalBalance?: number;
  slug: string;
};
