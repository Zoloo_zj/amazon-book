import express from "express";
import {
  createBook,
  deleteBook,
  editBook,
  getAllBooks,
  getBookById,
} from "../Controllers/book";

const router = express.Router();

router.route("/").get(getAllBooks).post(createBook);

router.route("/:id").get(getBookById).patch(editBook).delete(deleteBook);

export default router;
