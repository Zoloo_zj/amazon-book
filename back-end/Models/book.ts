import mongoose, { model, Document, Model } from "mongoose";
import { TBook } from "../Types/types";
import { slugify } from "transliteration";

export interface IBookModel extends Model<any> {
  computeAveragePrice(catId: string): any;
}

const MBookSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Нэрийг оруулна уу!"],
      unique: true,
      trim: true,
      maxLength: [50, "50 тэмдэгдээс хэтэрсэн байна!"],
    },
    price: { type: Number, required: [true, "Үнэ оруулна уу!"] },
    author: String,
    imageUrl: {
      type: String,
      default: "no-photo.jpg",
    },
    rating: {
      type: Number,
      min: [1, "Хамгийн багадаа 1 байх ёстой"],
      max: [5, "Хамгийн ихдээ 5 байх ёстой"],
    },
    description: {
      type: String,
      required: [true, "Тайлбар оруулна уу!"],
      maxLength: [500, "500 тэмдэгтээс хэтэрсэн байна!"],
    },
    categoryId: {
      type: mongoose.Schema.ObjectId,
      required: [true, "Ангилал сонгоно уу!"],
      ref: "Category",
    },
    totalbalance: Number,
    bestseller: { type: Boolean, default: false },
    slug: String,
  },
  { toJSON: { virtuals: true }, toObject: { virtuals: true } }
);

MBookSchema.statics.computeAveragePrice = async function (catId: string) {
  const obj = await this.aggregate([
    { $match: { categoryId: catId } },
    { $group: { _id: "$categoryId", avgPrice: { $avg: "$price" } } },
  ]);

  console.log(obj);

  return obj;
};

MBookSchema.post("save", function () {
  // @ts-ignore
  this.constructor.computeAveragePrice(this.categoryId);
});

MBookSchema.virtual("category", {
  ref: "Category",
  localField: "categoryId",
  foreignField: "_id",
});

MBookSchema.pre("save", function (next) {
  this.slug = slugify(this.name);
  next();
});

const MBooks = model<TBook & Document & IBookModel>("Book", MBookSchema);

export default MBooks;
