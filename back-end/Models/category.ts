import mongoose, { model, Document } from "mongoose";
import { TCategory } from "../Types/types";
import { slugify } from "transliteration";

const CategorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Нэрийг оруулна уу!"],
      unique: true,
      trim: true,
      maxLength: [50, "50 тэмдэгдээс хэтэрсэн байна!"],
    },
    description: {
      type: String,
      required: [true, "Тайлбар оруулна уу!"],
      maxLength: [300, "300 тэмдэгтээс хэтэрсэн байна!"],
    },
    photo: {
      type: String,
      default: "no-photo.jpg",
    },
    averageRating: {
      type: Number,
      min: [1, "Хамгийн багадаа 1 байх ёстой"],
      max: [10, "Хамгийн ихдээ 10 байх ёстой"],
    },
    averagePrice: Number,
    createdAt: {
      type: Date,
      default: Date.now,
    },
    slug: String,
  },
  { toJSON: { virtuals: true }, toObject: { virtuals: true } }
);

CategorySchema.virtual("books", {
  ref: "Book",
  localField: "_id",
  foreignField: "categoryId",
  justOne: false,
});

CategorySchema.pre(
  "deleteOne",
  { document: true, query: false },
  async function (next) {
    console.log("delete books...");
    await this.model("Book").deleteMany({ categoryId: this._id });

    next();
  }
);

CategorySchema.pre("save", function (next) {
  this.slug = slugify(this.name);
  this.averageRating = Math.floor(Math.random() * 10) + 1;
  // this.averagePrice = Math.floor(Math.random() * 100000) + 3000;
  next();
});

const MCategory = model<TCategory & Document>("Category", CategorySchema);

export default MCategory;
