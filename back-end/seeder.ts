import fs from "fs";
import { connectDB } from "./Config/db";
import MCategory from "./Models/category";
import MBook from "./Models/book";
import dotenv from "dotenv";
dotenv.config({ path: "./Config/config.env" });

connectDB();

const categories = JSON.parse(
  fs.readFileSync(`${__dirname}/data/categories.json`, "utf-8")
);

const books = JSON.parse(
  fs.readFileSync(`${__dirname}/data/books.json`, "utf-8")
);

const importData = async () => {
  try {
    await MCategory.create(categories);
    console.log("Өгөгдлийг импортоллоо!");
  } catch (error) {
    console.log(error);
  }
};
const importBooks = async () => {
  try {
    await MBook.create(books);
    console.log("Өгөгдлийг импортоллоо!");
  } catch (error) {
    console.log(error);
  }
};

const deleteData = async () => {
  try {
    await MCategory.deleteMany();
    console.log("Өгөгдлийг устгалаа!");
  } catch (error) {
    console.log(error);
  }
};

const deleteBooks = async () => {
  try {
    await MBook.deleteMany();
    console.log("Өгөгдлийг устгалаа!");
  } catch (error) {
    console.log(error);
  }
};

if (process.argv[2] === "-ic") {
  importData();
} else if (process.argv[2] === "-dc") {
  deleteData();
} else if (process.argv[2] === "-ib") {
  importBooks();
} else if (process.argv[2] === "-db") {
  deleteBooks();
}
