import { NextFunction, Request, Response } from "express";
import MyError from "../Utils/my-error";
import asyncHandler from "express-async-handler";
import MBook from "../Models/book";
import MCategory from "../Models/category";

export const getAllBooks = asyncHandler(async (req: Request, res: Response) => {
  let books = await MBook.find(req.query).populate({
    path: "category",
    select: "name averagePrice averageRating",
  });

  res.status(200).json({
    books,
  });
});

export const getBookById = asyncHandler(async (req: Request, res: Response) => {
  const id = req.params.id;
  const book = await MBook.findById(id);
  if (!book) {
    throw new MyError(`${req.params.id} ID-тай ном байхгүй байна!`, 400);
  }
  res.status(200).json({
    success: true,
    data: book,
  });
});

export const createBook = asyncHandler(async (req: Request, res: Response) => {
  const categoryId = req.body.categoryId;

  const category = await MCategory.findById(categoryId);

  if (!category) {
    throw new MyError(categoryId + "ID-тай Категори байхгүй байна!", 400);
  }

  const book = await MBook.create(req.body);

  res.status(200).json({
    success: true,
    book,
  });
});

export const editBook = asyncHandler(async (req: Request, res: Response) => {
  const id = req.params.id;
  const body = req.body;
  const book = await MBook.findByIdAndUpdate(id, body, {
    new: true,
    runValidators: true,
  });
  if (!book) {
    throw new MyError(`${req.params.id} ID-тай ном байхгүй байна!`, 400);
  }
  res.status(200).json({
    success: true,
    data: book,
  });
});

export const deleteBook = asyncHandler(async (req: Request, res: Response) => {
  const id = req.params.id;
  const book = await MBook.findByIdAndDelete(id);
  if (!book) {
    throw new MyError(`${req.params.id} ID-тай ном байхгүй байна!`, 400);
  }
  res.status(200).json({
    success: true,
    data: book,
  });
});
