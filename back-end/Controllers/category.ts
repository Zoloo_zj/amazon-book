import { NextFunction, Request, Response } from "express";
import MyError from "../Utils/my-error";
import asyncHandler from "express-async-handler";
import MCategory from "../Models/category";

export const getAllCategories = asyncHandler(
  async (req: Request, res: Response) => {
    const page = Number(req.query.page);
    const limit = Number(req.query.limit);
    const select = req.query.select;
    const sort = req.query.sort;

    ["page", "limit", "sort", "select"].forEach((el) => delete req.query[el]);

    // Pagination

    const total = await MCategory.countDocuments();
    const pageCount = Math.ceil(total / limit);
    const start = (page - 1) * limit + 1;
    let end = start + limit - 1;
    if (end > total) end = total;
    const pagination = {
      nextPage: 0,
      prevPage: 0,
      total,
      pageCount,
      start,
      end,
    };

    if (page < pageCount) pagination.nextPage = page + 1;

    if (page > 1) pagination.prevPage = page - 1;

    console.log(req.query, select, sort);
    const categories = await MCategory.find(req.query, select)
      .sort(sort as string)
      .skip(start - 1)
      .limit(limit);

    res.status(200).json({
      success: true,
      data: categories,
      pagination,
    });
  }
);

export const editCategory = asyncHandler(
  async (req: Request, res: Response) => {
    const id = req.params.id;
    const body = req.body;

    const category = await MCategory.findByIdAndUpdate(id, body, {
      new: true,
      runValidators: true,
    });
    if (!category) {
      throw new MyError(`${req.params.id} ID-тай категори байхгүй!`, 400);
    }
    res.status(200).json({
      success: true,
      data: category,
    });
  }
);

export const deleteCategory = asyncHandler(
  async (req: Request, res: Response) => {
    const id = req.params.id;

    const category = await MCategory.findById(id);
    if (!category) {
      throw new MyError(`${req.params.id} ID-тай категори байхгүй!`, 400);
    }

    await category.deleteOne();

    res.status(200).json({
      success: true,
      data: category,
    });
  }
);

export const createCategory = asyncHandler(
  async (req: Request, res: Response) => {
    const category = await MCategory.create(req.body);

    res.status(200).json({
      success: true,
      category,
    });
  }
);

export const getCategoryById = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id;
    const category = await MCategory.findById(id).populate("books");

    if (!category) {
      throw new MyError(`${req.params.id} ID-тай категори байхгүй!`, 400);
    }
    res.status(200).json({
      success: true,
      data: category,
    });
  }
);
