import { ErrorRequestHandler } from "express";

const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
  const error = { ...err };
  error.message = err.message;

  if (error.name === "CastError") {
    error.message = "ID дамжуулна уу!";
    error.statusCode = 400;
  }

  res.status(error.statusCode || 500).json({
    success: false,
    error: error,
  });
};

export default errorHandler;
