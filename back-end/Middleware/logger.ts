import { NextFunction, Request, Response } from "express";
import morgan from "morgan";
import fs from "fs";
import path from "path";

var accessLogStream = fs.createWriteStream(path.join(__dirname, "access.log"), {
  flags: "a",
});

export const logger = (req: Request, res: Response, next: NextFunction) => {
  morgan("combined", { stream: accessLogStream });
  next();
};
